# WriteBetter - English text linter [![Netlify Status](https://api.netlify.com/api/v1/badges/9cc442c7-3e70-4f91-9a2f-b2b06f0d3b3d/deploy-status)](https://app.netlify.com/sites/writebetter/deploys)

## [Demo site](https://writebetter.netlify.com/)

## Purpose

This simple text linter will correct simple grammar mistakes, and make your English text sparkle.  

Originally, I built it to lint my README.md files. Us developers are not that good with grammar 😊.  
After using it for a while on Github repos, I decided to generalize it.

## Usage

Either use it on the [demo website](https://writebetter.netlify.com/), or clone it and deploy to your own server.

## Modules used:

1. [write-good](https://github.com/btford/write-good) - grammar checks
1. [create-react-app](https://github.com/facebook/create-react-app) - scaffolding the app
1. [reactsrap](https://github.com/reactstrap/reactstrap) - Bootstrap components
1. [randomcolor](https://github.com/davidmerfield/randomColor) - generate the results colors

## The future

I hope to add a more "serious" grammar checks (a-la Grammarly), as well as spelling and syntax checks.  
Leave comments or requests in the [Issues](https://gitlab.com/TravelingTechGuy/writebetter/issues).
