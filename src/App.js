import React, {useState} from 'react';
import TextInput from './TextInput';
import Results from './Results';
import './App.css';

const Link = ({url, text}) => <a href={url} target="_blank" rel="noopener noreferrer" className="App-link">{text}</a>;

export default () => {
  const [payload, setPayload] = useState(null);
  
  return (
    <div className="App">
      <header className="App-header">
        Write Better <small>Lint your English text</small>
      </header>
      <div className="App-content">
        {
          payload 
          ?
          <Results payload={payload} onReset={() => setPayload(null)} />
          : 
          <TextInput onLint={payload => setPayload(payload)} />
        }
      </div>
      <footer className="App-footer">
        Developed by{' '}<Link url="https://www.TravelingTechGuy.com" text="Traveling Tech Guy" />
        {' '}
        &copy; {(new Date()).getFullYear()} and licensed with MIT license.
        {' '}
        <Link url="https://gitlab.com/TravelingTechGuy/writebetter" text="Fork it on Gitlab" />
      </footer>
    </div>
  );
};
