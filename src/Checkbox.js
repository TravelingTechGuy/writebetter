import React, {useState} from 'react';
import {Input, Label} from 'reactstrap';

export default ({name, label, checked, onCheck}) => {
  const [value, setValue] = useState(checked);

  const onChange = e => {
    let val = e.target.checked;
    setValue(val);
    onCheck(name, val);
  }

  return <>
    <Label check>
      <Input type="checkbox" checked={value} onChange={onChange} />
      {label}
    </Label>
  </>
};
