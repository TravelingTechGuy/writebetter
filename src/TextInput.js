import React, {useState} from 'react';
import {Button, Col, Form, FormGroup, FormText, Input, Label} from 'reactstrap';
import Checkbox from './Checkbox';
import './TextInput.css';

const DEFAULT_OPTIONS = {
  passive: true,
  illusion: true,
  so: true,
  thereIs: true,
  weasel: true,
  adverb: true,
  tooWordy: true,
  cliches: true,
  eprime: false
};

export default ({onLint}) => {
  const [text, setText] = useState('');
  const [options, setOptions] = useState(DEFAULT_OPTIONS);
  
  const onCheck = (field, value) => setOptions(Object.assign(options, {[field]: value}));

  const submit = e => {
    e.preventDefault();
    onLint({text, options})
  }

  return <div>
      <Form className="form">
        <div className="options">
          <Label>Select the attributes you'd like to test your text against:</Label>
          <FormGroup row>
            <Col>
              <Checkbox name="passive" label="Passive voice" checked={options.passive} onCheck={onCheck} />
            </Col>
            <Col>
              <Checkbox name="thereIs" label="There Is/Are at the beginning of the sentence" checked={options.thereIs} onCheck={onCheck} />
            </Col>
            <Col>
              <Checkbox name="tooWordy" label="Wordy phrases and unnecessary words" checked={options.tooWordy} onCheck={onCheck} />
            </Col>
          </FormGroup>
          <FormGroup row>
            <Col>
              <Checkbox name="illusion" label="Lexical illusions" checked={options.illusion} onCheck={onCheck} />
            </Col>
            <Col>
              <Checkbox name="adverb" label="Adverbs that can weaken meaning" checked={options.adverb} onCheck={onCheck} />
            </Col>
            <Col>
              <Checkbox name="cliches" label="Common cliches" checked={options.cliches} onCheck={onCheck} />
            </Col>
          </FormGroup>
          <FormGroup row>
            <Col>
              <Checkbox name="so" label="'So' at the beginning of the sentence" checked={options.so} onCheck={onCheck} />
            </Col>
            <Col>
              <Checkbox name="weasel" label="'Weasel' Words" checked={options.weasel} onCheck={onCheck} />
            </Col>
            <Col>
              <Checkbox name="eprime" label="'To-Be' (Eprime)" checked={options.eprime} onCheck={onCheck} />
            </Col>
          </FormGroup>
        </div>
        <div>
          <Label>Please paste the text you want to lint:</Label>
          <Input type="textarea" className="text" onChange={e => setText(e.target.value)} />
        </div>
        <div className="submit">
          <Button color="info" onClick={submit}>Lint my text</Button>
        </div>
        <FormText className="formLabel"><strong>Important:</strong> Do not use this tool to be a jerk to other people about their writing.</FormText>
      </Form>
    </div>;
}
