import React from 'react';
import writeGood from 'write-good';
import randomColor from 'randomcolor';
import {Button} from 'reactstrap';
import './Results.css';

export default ({payload, onReset}) => {
  const {text, options} = payload;
  let suggestions = writeGood(text, options);
  let colors = randomColor({luminosity: 'light', hue: 'random', count: suggestions.length});

  const buildResultHTML = () => {
    let loc = 0, result = '';

    for(let i = 0; i < suggestions.length; i++) {
      let point = suggestions[i];
      result += text.substring(loc, point.index);
      result += `<span style="background-color: ${colors[i]}" title="suggetion ${i + 1}">`;
      result += text.substring(point.index, point.index + point.offset);
      result += '</span>';
      loc = point.index + point.offset;
    }
    if(loc < text.length) {
      result += text.substring(loc);
    }
    return {__html: result};
  }

  return <div className="resultText">
    <div className="text" dangerouslySetInnerHTML={buildResultHTML()}></div>
    <div className="suggestions">
      {
        suggestions && <ol>
          {suggestions.map((suggestion, i) => 
            <li key={50 + i}>
              <span style={{backgroundColor: colors[i]}}>{suggestion.reason}</span>
            </li>
          )}
        </ol>
      }
    </div>
    <div className="reset">
      <Button size="sm" color="info" onClick={onReset}>Reset</Button>
    </div>
  </div>
}
